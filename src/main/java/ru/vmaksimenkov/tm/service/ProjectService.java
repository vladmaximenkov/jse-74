package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.repository.ProjectRepository;
import ru.vmaksimenkov.tm.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    private final ProjectRepository repository;

    @NotNull
    private final UserRepository userRepository;

    @Autowired
    public ProjectService(@NotNull final ProjectRepository repository, @NotNull final UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @NotNull
    public Collection<Project> findAll() {
        return repository.findAll();
    }

    @Nullable
    public Collection<Project> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    public void merge(@NotNull final List<Project> list) {
        list.forEach(repository::save);
    }

    @Transactional
    public void merge(@NotNull final Project project) {
        repository.save(project);
    }

    public void merge(@NotNull final String login, @NotNull final Project project) {
        project.setUser(userRepository.findByLogin(login));
        merge(project);
    }

    public void removeAll() {
        repository.deleteAll();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    @Transactional
    public void removeById(@NotNull final String id) {
        repository.deleteById(id);
    }

}
