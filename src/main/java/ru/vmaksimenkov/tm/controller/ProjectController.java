package ru.vmaksimenkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.CustomUser;
import ru.vmaksimenkov.tm.service.ProjectRecordService;

@Controller
public class ProjectController {

    @Autowired
    private ProjectRecordService service;

    @GetMapping("/project/create")
    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    public String create(@AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user) {
        @NotNull final ProjectRecord project = new ProjectRecord("New project " + System.currentTimeMillis());
        project.setUserId(user.getId());
        service.merge(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    public String delete(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id
    ) {
        service.removeByUserIdAndId(user.getId(), id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    public ModelAndView edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id
    ) {
        @Nullable final ProjectRecord project = service.findByUserIdAndId(user.getId(), id);
        return new ModelAndView("project-edit", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    public String edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @ModelAttribute("project") ProjectRecord project,
            BindingResult result
    ) {
        project.setUserId(user.getId());
        service.merge(project);
        return "redirect:/projects";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

}
