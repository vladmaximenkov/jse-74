package ru.vmaksimenkov.tm.exception;

public class RoleNotFoundException extends AbstractException {

    public RoleNotFoundException() {
        super("Error! Role not found...");
    }

}
