package ru.vmaksimenkov.tm.enumerated;

public enum EventType {

    LOAD,
    START_PERSIST,
    FINISH_PERSIST,
    START_UPDATE,
    FINISH_UPDATE,
    START_REMOVE,
    FINISH_REMOVE

}
