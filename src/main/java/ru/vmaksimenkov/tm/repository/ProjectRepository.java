package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends AbstractBusinessRepository<Project> {

    @Modifying
    @Query("DELETE FROM Project WHERE id = :userId AND name = :name")
    Long deleteByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @Query("SELECT id FROM Project WHERE userId = :userId AND name = :name")
    @Nullable String getIdByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    boolean existsByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @Nullable Project findByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    void deleteByUserId(@NotNull @Param("userId") String userId);

    void deleteByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    boolean existsByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable List<Project> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable Project findByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull Long countByUserId(@NotNull @Param("userId") String userId);

}
