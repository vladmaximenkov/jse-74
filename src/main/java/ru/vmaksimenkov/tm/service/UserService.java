package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.enumerated.RoleType;
import ru.vmaksimenkov.tm.model.Role;
import ru.vmaksimenkov.tm.model.User;
import ru.vmaksimenkov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Collections;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("test", "test", RoleType.USER);
    }

    private void initUser(@NotNull final String login, @NotNull final String password, @NotNull final RoleType roleType) {
        @Nullable final User user = repository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Transactional
    public void createUser(@NotNull final String login, @NotNull final String password, @NotNull final RoleType roleType) {
        if (isEmpty(login) || isEmpty(password)) return;
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        repository.save(user);
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
        return repository.findByLogin(login);
    }

}