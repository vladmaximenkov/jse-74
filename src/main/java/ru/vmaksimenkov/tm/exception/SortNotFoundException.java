package ru.vmaksimenkov.tm.exception;

public class SortNotFoundException extends AbstractException {

    public SortNotFoundException() {
        super("Error! Sort not found...");
    }

}
