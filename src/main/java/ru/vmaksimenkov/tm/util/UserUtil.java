package ru.vmaksimenkov.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.vmaksimenkov.tm.model.CustomUser;

import java.nio.file.AccessDeniedException;

public class UserUtil {

    @SneakyThrows
    public static String getUserId() {
        @NotNull final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        @Nullable final Object principal = authentication.getPrincipal();
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException("");
        @NotNull final CustomUser customUser = (CustomUser) principal;
        return customUser.getId();
    }

}
