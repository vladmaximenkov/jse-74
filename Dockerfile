FROM openjdk:8-jre-alpine3.9

COPY target/tm-0.0.1-SNAPSHOT.war /tm.war

EXPOSE 8080

CMD ["java", "-jar", "/tm.war"]